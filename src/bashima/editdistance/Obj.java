package bashima.editdistance;

public class Obj {
	public int score;
	public String str;
	
	public Obj(){
		score =0;
		str="";
	}

	public int getScore() {
		return score;
	}

	public String getStr() {
		return str;
	}
	
}
