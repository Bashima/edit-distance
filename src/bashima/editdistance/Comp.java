package bashima.editdistance;

import java.util.Comparator;

public class Comp implements Comparator<Obj>{
	public int compare(Obj o1, Obj o2)
	{
		return o1.score-o2.score;
	}

}
