package bashima.editdistance;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class EditDistance {

	public int matrix[][];
	public int action[][];
	public String a, b;
	public int length1, lenght2;
	int left, top, digonal;

	public EditDistance(String aq, String bq) {
		a = aq;
		b = bq;
		length1 = a.length();
		lenght2 = b.length();
		matrix = new int[length1 + 1][lenght2 + 1];
		action = new int[length1][lenght2];
		for (int i = 0; i <= length1; i++) {
			matrix[i][0] = i;
		}
		for (int j = 0; j <= lenght2; j++) {
			matrix[0][j] = j;
		}
		left = 0;
		top = 0;
		digonal = 0;
	}

	public void printMatrix() {
		for (int i = 0; i <= length1; i++) {
			for (int j = 0; j <= lenght2; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.print("\n");
		}
	}

	public String convert(int x) {
		String str = "";
		switch (x) {
		case 0:
			str = "No action";
			break;
		case 1:
			str = "Insert";
			break;
		case 2:
			str = "Delete";
			break;
		case 3:
			str = "Substitution";
			break;
		default:
			break;
		}
		return str;
	}

	public void printAction() {
		for (int i = 0; i < length1; i++) {
			for (int j = 0; j < lenght2; j++) {
				System.out.print(convert(action[i][j]) + "\t");
				// System.out.print(action[i][j] + "\t");
			}
			System.out.print("\n");
		}
	}

	public int solver() {
		for (int i = 1; i <= length1; i++) {
			for (int j = 1; j <= lenght2; j++) {
				int cost = 0;
				if (a.charAt(i - 1) == b.charAt(j - 1)) {
					cost = 0;
				} else {
					cost = 1;
				}
				digonal = matrix[i - 1][j - 1] + cost;
				left = matrix[i][j - 1] + 1;
				top = matrix[i - 1][j] + 1;
				matrix[i][j] = Math.min(Math.min(digonal, left), top);
				if (a.charAt(i - 1) == b.charAt(j - 1)) {
					action[i - 1][j - 1] = 0;
				} else {
					if (matrix[i][j] == digonal) {
						action[i - 1][j - 1] = 3;
					}
					if (matrix[i][j] == top) {
						action[i - 1][j - 1] = 2;
					}
					if (matrix[i][j] == left) {
						action[i - 1][j - 1] = 1;
					}
				}
			}
		}
		return matrix[length1][lenght2];
	}

	public void printPath() {
		int i = length1 - 1;
		int j = lenght2 - 1;
		System.out.println("(" + i + "," + j + "):  " + convert(action[i][j]));
		while (true) {
			if (action[i][j] == 0) {
				i = i - 1;
				j = j - 1;
			} else if (action[i][j] == 1) {
				j = j - 1;
			} else if (action[i][j] == 2) {
				i = i - 1;
			} else if (action[i][j] == 3) {
				i = i - 1;
				j = j - 1;
			}
			System.out.println("(" + i + "," + j + "):  "
					+ convert(action[i][j]));
			if (i == 0 && j == 0)
				break;
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		// String aq = "";
		// String bq = "";
		ArrayList<String> refer = new ArrayList<String>();
		ArrayList<String> test = new ArrayList<String>();
		Scanner scan_ref = new Scanner(new File("Reference.txt"));
		for (int i = 0; i < 19; i++) {
			refer.add(scan_ref.nextLine());
		}
		scan_ref.close();
		Scanner scan_test;
			scan_test = new Scanner(new File("Test.txt"));
		for (int i = 0; i < 7; i++) {
			
			test.add(scan_test.nextLine());
		}
		scan_test.close();
		for (String test_element : test) {
			ArrayList<Obj> ob = new ArrayList<Obj>();
			for (String ref_element : refer) {
				EditDistance ed = new EditDistance(test_element, ref_element);
				Obj o = new Obj();
				o.score= ed.solver();
				o.str = ref_element;
				ob.add(o);
				}
//			for(int k=0; k<refer.size();k++)
//			{
//				System.out.println(test_element + " "+ refer.get(k)+" "+ score.get(k));
//			}
//			for (Obj t : ob) {
//				System.out.println(test_element + " "+ t.str+" "+ t.score);
//			}
			Collections.sort(ob, new Comp());
			for (Obj t : ob) {
				System.out.println(test_element + " "+ t.str+" "+ t.score);
			}
		}
		

		// ed.printMatrix();
		// ed.printAction();
		// ed.printPath();
	}

}
